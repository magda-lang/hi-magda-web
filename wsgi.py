from wsgiref.util import setup_testing_defaults
from wsgiref.simple_server import make_server

import subprocess
from hashlib import sha1

cont_type = lambda x: {
    'css':'text/css',
    'html':'text/html',
    'js':'application/x-javascript',
    'svg':'image/svg+xml',
    'magda':'text/magda',
}[x]

MAGDA_BIN = "./hi-magda/bin/magda"
FILES_DIR = "files/"

class RunTimeError(Exception): pass
class FileNotFoundError(Exception) : pass

def run_interpreter(env, start_response):
    source_code_length = int(env['CONTENT_LENGTH']) 
    source_code = env['wsgi.input'].read(source_code_length)    
    filename = sha1(source_code).hexdigest()
    fpath = "%s%s.magda" % (FILES_DIR,filename)
    
    f = open(fpath,"wb")
    f.write(source_code)
    f.close()
    try:
        res = subprocess.check_output([MAGDA_BIN, fpath],timeout=5)
    except Exception as e:
        raise RunTimeError("Runtime error")
    
    status = '200 OK'
    headers = [('Content-type', 'text/plain')]    
    start_response(status, headers)
    return [res]

def simple_app(env, start_response):
    #res = "%s %s\n" % (env['REQUEST_METHOD'], env['PATH_INFO'])
    try:
        if env['PATH_INFO'] == "/i/" and env['REQUEST_METHOD'] == "POST":
            return run_interpreter(env,start_response)
        if env['PATH_INFO'] == "/":
            env['PATH_INFO'] = "/index.html"
        if not env['REQUEST_METHOD'] == "GET":
            raise FileNotFoundError()
        file_ext = env['PATH_INFO'].split('.')[-1]
        if "/examples/" in env['PATH_INFO']:
            env['PATH_INFO'] = "hi-magda" + env['PATH_INFO']
        else:
            env['PATH_INFO'] = "html" + env['PATH_INFO']        
        res = open(env['PATH_INFO']).read()
        status = '200 OK'
        headers = [('Content-type', cont_type(file_ext))]
    except FileNotFoundError:
        res = "Not Found"
        status = '404 '
        headers = [('Content-type', 'text/plain')]
    except RunTimeError as e:
        res = str(e)
        status = '400 '
        headers = [('Content-type', 'text/plain')]
    except Exception as e:
        res = str(e)
        status = '500 '
        headers = [('Content-type', 'text/plain')]
    start_response(status, headers)
    return [bytes(res,'ascii')]

httpd = make_server('', 8000, simple_app)
print("Serving on port 8000...")
httpd.serve_forever()
